## INTRODUCTION

The entity_print_typeset module integrates [https://typeset.sh](typeset.sh) with Entity Print.


## REQUIREMENTS

You need a valid subscription for typeset. Run these commands on your drupal project:

```
composer config repositories.typesetsh composer https://packages.typeset.sh
composer config -g http-basic.packages.typeset.sh "{PUBLIC_ID}" "{TOKEN}"
composer require typesetsh/typesetsh
```

(More info here: https://typeset.sh/en/documentation/php)

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

Currently there's nothing to configure. Happy to review and accept Merge Requests.

## MAINTAINERS

Current maintainers for Drupal 10:

- Stephan Huber (stmh) - https://www.drupal.org/u/stmh
- Supported by Factorial GmbH - https://www.drupal.org/factorial-gmbh

